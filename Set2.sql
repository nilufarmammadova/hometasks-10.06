--Set 2
--DDL Operation (ALTER)
--Write a SQL statement to add a column 'middle_name' to the 'employees' table.
ALTER TABLE employees ADD middle_name VARCHAR2(50);

--DML Operation (UPDATE)
--Write a SQL statement to increase the salary of all employees by 10%. 
UPDATE employees
SET
    salary = salary + ( salary * 10 / 100 );
    
--SELECT Statement with WHERE clause
--Write a SQL statement to select all employees who have a salary greater than $5000. 
SELECT
    *
FROM
    employees
WHERE
    salary > 5000;
    
--Single Function (Number)
--Write a SQL statement to round the salary of all employees to the nearest whole number.
SELECT
    round(salary)
FROM
    employees;

--Conversion Function
--Write a SQL statement to convert the 'hire_date' of all employees to the format 'YYYY-MM-DD'. 
SELECT
    to_char(hire_date, 'YYYY-MM-DD')
FROM
    employees;
