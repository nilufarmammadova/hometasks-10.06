--Set 3
--DDL Operation (DROP)
--Write a SQL statement to drop the 'new_employees' table. 
DROP TABLE new_employees;

--DML Operation (DELETE)
--Write a SQL statement to delete all employees who are working in department id 10. 
DELETE FROM employees
WHERE
    department_id = 10;

commit;

--Subquery
--Write a SQL statement to find the names of all employees who have a higher salary than the average salary.
SELECT
    *
FROM
    employees
WHERE
    salary > (
        SELECT
            AVG(salary)
        FROM
            employees
    );

--Set Operator (UNION)
--Write a SQL statement to display a list of all unique department IDs from both the 'employees'
--and 'departments' table.
SELECT
    department_id
FROM
    employees union
SELECT
    department_id
FROM
    departments;
    
--Join (INNER JOIN)
--Write a SQL statement to display the employee's full name and department name for all employees. 
SELECT
    first_name
    || ' '
    || last_name "Full Name",
    department_name
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id;
