--Set 10
--SELECT with Self JOIN
--Write a SQL statement to find pairs of employees who have the same job title. 
SELECT
    distinct e.first_name as employee_name, m.first_name as employee_name1
FROM
         employees e
    INNER JOIN employees m ON e.job_id = m.job_id; 

--SELECT with Aggregate Function and WHERE clause
--Write a SQL statement to find the total salary of employees whose department id is 30. 
SELECT
    department_id,
    SUM(salary)
FROM
    employees
WHERE
    department_id = 30
GROUP BY
    department_id;

--SELECT with UNION ALL clause
--Write a SQL statement to combine the list of all department IDs in 'employees' and 'departments' table
--including duplicates.
SELECT
    department_id
FROM
    employees
UNION ALL
SELECT
    department_id
FROM
    departments;

--SELECT with NOT IN clause
--Write a SQL statement to select all employees who do not work in department 30, 50 or 80. 
SELECT
    *
FROM
    employees
where department_id not
in(30, 50, 80);

--SELECT with Subquery in FROM clause
--Write a SQL statement to select the highest paid employee from each department.
SELECT
    department_id,
    MAX(salary)
FROM
    employees
GROUP BY
    department_id;
