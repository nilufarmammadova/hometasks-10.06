--Set 9
--SELECT with JOIN and USING clause
--Write a SQL statement to display the employee's full name and department name using the 'department_id'
--field.
SELECT
    first_name
    || ' '
    || last_name "Full Name",
    department_name
FROM
    employees e
JOIN departments d USING ( DEPARTMENT_ID );
    
--SELECT with CASE statement
--Write a SQL statement to categorize employees' salaries into 'Low', 'Medium' and 'High'. 
SELECT
    salary,
    CASE
        WHEN salary BETWEEN 0 AND 1000     THEN
            'Low'
        WHEN salary BETWEEN 1000 AND 10000  THEN
            'Medium'
        WHEN salary BETWEEN 10000 AND 30000 THEN
            'High'
        ELSE
            'Unknown'
    END AS category
FROM
    employees; 

--SELECT with Subquery in SELECT clause
--Write a SQL statement to display the department name and the highest salary in each department. 
SELECT
    department_name,
    MAX(salary)
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id
GROUP BY
    department_name;

--SELECT with WHERE and NULL clause
--Write a SQL statement to find all employees with no commission_pct. 
SELECT
    *
FROM
    employees
where
    commission_pct is NOT NULL;
    
--SELECT with GROUP BY and ROLLUP clause
--Write a SQL statement to show the total salary, grouped by department and job title. 
SELECT
    department_id,
    job_id,
    SUM(salary)
FROM
    employees
GROUP BY
    department_id,
    job_id;
