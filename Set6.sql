--Set 6
--SELECT with DISTINCT clause
--Write a SQL statement to select all distinct job titles from the 'employees' table. 
SELECT
    distinct job_id
FROM
    employees;
    
--SELECT with ALIAS
--Write a SQL statement to display the 'employee_id' as 'Employee ID' and 'first_name' as 'First Name'
--from the 'employees' table. 
SELECT
    employee_id "Employee ID",
    first_name  "First Name"
FROM
    employees;
    
--SELECT with ORDER BY clause
--Write a SQL statement to select all employees' first names, sorted in ascending order. 
SELECT
    first_name
FROM
    employees
ORDER BY
    first_name;
    
--SELECT with GROUP BY clause
--Write a SQL statement to find the total salary for each job title. 
SELECT
    job_id,
    SUM(salary)
FROM
    employees
GROUP BY
    job_id;
    
--SELECT with LIMIT clause
--Write a SQL statement to select the top 5 highest earning employees. 
SELECT
    *
FROM
    employees
ORDER BY
    salary DESC
FETCH FIRST 5 ROWS ONLY; 
