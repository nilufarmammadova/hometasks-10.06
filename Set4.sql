--Set 4
--DDL Operation (CREATE INDEX)
--Write a SQL statement to create an index on the salary column in the 'employees' table. 
create index salary_index on
employees(salary);

--DML Operation (INSERT with SELECT)
--Write a SQL statement to insert into a new table 'high_salary_employees' all employees
--who have a salary greater than $10000.
CREATE TABLE high_salary_employees
    AS
        SELECT
            *
        FROM
            employees
        WHERE
            salary > 10000; 
commit;

--SELECT Statement with ORDER BY clause
--Write a SQL statement to select all employees' first and last names and their salaries,
--sorted by salary in descending order.
SELECT
    first_name
    || ' '
    || last_name "Full Name",
    salary
FROM
    employees
ORDER BY
    salary DESC;
    
--Single Function (Date)
--Write a SQL statement to calculate the number of years between the hire date and current date
--for all employees.
SELECT
    EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM hire_date)
FROM
    employees;
    
--Aggregate Function with GROUP BY
--Write a SQL statement to find the total salary paid for each department. 
SELECT
    department_id,
    SUM(salary)
FROM
    employees
GROUP BY
    department_id;
