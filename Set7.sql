--Set 7
--SELECT with JOIN and ON clause
--Write a SQL statement to display the employee's full name, department name, and location id. 
SELECT
    first_name
    || ' '
    || last_name "Full Name",
    department_name,
    location_id
FROM
         employees e
    JOIN departments d ON e.department_id = d.department_id;

--SELECT with WHERE and LIKE clause
--Write a SQL statement to find all employees whose first name starts with 'A'. 
SELECT
    *
FROM
    employees
where first_name like 'A%';

--SELECT with WHERE and IN clause
--Write a SQL statement to find all employees who work in department 10, 20 or 30. 
select * from employees 
where
    department_id in(10, 20, 30);
    
--SELECT with WHERE and BETWEEN clause
--Write a SQL statement to find all employees whose salary is between $5000 and $10000. 
SELECT
    *
FROM
    employees
WHERE
    salary BETWEEN 5000 AND 10000;
    
--SELECT with Aggregate Function and HAVING clause
--Write a SQL statement to find the job titles with more than 5 employees. 
SELECT
    job_id,
    COUNT(employee_id)
FROM
    employees having
COUNT(employee_id) > 5
group by job_id; 
