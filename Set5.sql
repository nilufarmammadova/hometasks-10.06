--Set 5
--DDL Operation (ALTER TABLE MODIFY COLUMN)
--Write a SQL statement to modify the data type of 'commission_pct' column in 'employees' table
--to NUMBER(3, 2). 
ALTER TABLE employees modify commission_pct number (3,2); 

--DML Operation (UPDATE with WHERE)
--Write a SQL statement to update the department id to 20 for all employees whose last name is 'King'. 
UPDATE employees
SET
    department_id = 20
WHERE
    last_name = 'King';
    commit;
--Subquery in FROM clause
--Write a SQL statement to find the maximum salary in each department. 
SELECT
    department_id,
    MAX(salary)
FROM
    employees
GROUP BY
    department_id;
    
--Set Operator (MINUS)
--Write a SQL statement to find all department IDs that are in the 'departments' table
--but not in the 'employees' table. 
SELECT
    department_id
FROM
    departments
MINUS
SELECT
    department_id
FROM
    employees;
    
--Join (LEFT OUTER JOIN)
--Write a SQL statement to display the department name for all employees, including those without a
--department.
SELECT
    e.employee_id,
    d.department_name
FROM
    employees   e
    LEFT JOIN departments d ON e.department_id = d.department_id;
