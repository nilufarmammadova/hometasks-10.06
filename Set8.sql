--Set 8
--SELECT with multiple JOINS
--Write a SQL statement to display the employee's name, their manager's name and the department name.  
SELECT
    e.first_name AS employee_name,
    m.first_name AS manager_name,
    d.department_name
FROM
         employees e
    JOIN employees   m ON e.manager_id = m.employee_id
    JOIN departments d ON e.department_id = d.department_id;

--SELECT with WHERE and OR clause
--Write a SQL statement to select all employees who are either 'IT_PROG' or 'SA_REP'. 
SELECT
    *
FROM
    employees
WHERE
    job_id = 'IT_PROG'
    OR job_id = 'SA_REP';

--SELECT with Aggregate Function and GROUP BY clause
--Write a SQL statement to find the average salary for each job title. 
SELECT
    job_id,
    AVG(salary)
FROM
    employees
GROUP BY
    job_id;

--SELECT with Subquery in WHERE clause
--Write a SQL statement to select all employees who earn more than the average salary. 
SELECT
    *
FROM
    employees
WHERE
    salary > (
        SELECT
            AVG(salary)
        FROM
            employees
    );

--SELECT with UNION clause
--Write a SQL statement to combine the list of all department IDs in 'employees' and 'departments' table.
SELECT
    department_id
FROM
    employees
UNION
SELECT
    department_id
FROM
    departments; 
