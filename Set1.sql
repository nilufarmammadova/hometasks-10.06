--Set 1
--DDL Operation (CREATE)
--Write a SQL statement to create a new table named 'new_employees' that has the same structure as the
--'employees' table.
CREATE TABLE new_employees
    AS
        SELECT
            *
        FROM
            employees
        WHERE
            1 = 2;

--DML Operation (INSERT)
--Write a SQL statement to insert a new record into the 'employees' table. Use your own values for the data.
Insert Into new_employees (employee_id,
    first_name,
    last_name,
    email,
    phone_number,
    hire_date,
    job_id,
    salary,
    commission_pct ,
    manager_id,
    department_id)
    values (1, 'Jannathon', 'Levy', 'j.levy@gmail.com', 0998752661, '10-12-2022', 1, 12500, 0.2, 1, 1);
commit;

--SELECT Statement
--Write a SQL statement to select all employees' first and last names and their department ids from the
--'employees' table.
SELECT
    first_name,
    last_name,
    department_id
FROM
    employees;
    
--Single Function (Character)
--Write a SQL statement to display the first name of all employees in lower case.
SELECT
    lower(first_name)
FROM
    employees;
    
--Aggregate Function
--Write a SQL statement to find the average salary in the 'employees' table.
SELECT
    AVG(salary)
FROM
    employees;
